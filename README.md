Overview
========

This package contains a set of funcions and script helpers for common
functionality for Node-RED.

Installation
============

Remove you current `libs` directory and link this repo to it (or clone
it there):

    $ rm ~/.node-red/lib -rf
    $ ln -s ~/repos/node-red-support/ lib
