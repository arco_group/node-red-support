// name: invert
// outputs: 1
msg.payload = !msg.payload;
return msg;